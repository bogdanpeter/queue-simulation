package queue;
/*
 * Homework2 Queue simulation
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */
public class Clients {
	private volatile int id;
	private long arrivalTime; // the time at which the client arrived at the queue
	private int waitingTime; // the time the client will have to wait in the queue until its his time to be "served"
	private int servingTime; // the time the client will have to stay at the Cashier once its his time to be "served"
	private static int newId =1; // we generate the id's of the clients with a 
								// static variable
	public Clients()
	{
		this.id = newId++;
		this.servingTime = 0;
	}
	public Clients(int servingTime, long arrival)
	{
		this.id = newId++;
		this.arrivalTime = arrival;
		this.servingTime = servingTime;
	}
	public int getId()
	{
		return this.id;
	}
	public void setWait(int wait)
	{
		this.waitingTime = wait;
	}
	public int getWait()
	{
		return this.waitingTime;
	}
	public long getArrivalTime()
	{
		return this.arrivalTime;
	}
	public int getServingTime()
	{
		return this.servingTime;
	}
}
