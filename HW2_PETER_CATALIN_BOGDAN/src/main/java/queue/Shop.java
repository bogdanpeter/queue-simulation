package queue;
/*
 * Homework2 Queue simulation
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */
import java.util.concurrent.TimeUnit;

import gui.Controller;
import gui.View;


public class Shop {
	private static long startingTime;
	
	public static void setTime() // we set the starting time as the time when the Start! button is pressed
	{
	startingTime = TimeUnit.SECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS);
	}
	public static long getTime() // we compute the current time based on the current pc time and the starting point
	{
		return TimeUnit.SECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS) - startingTime;
	}
	public static void main(String[] args){

		View view = new View();
		Controller controller = new Controller(view);
		
		
	}
}
