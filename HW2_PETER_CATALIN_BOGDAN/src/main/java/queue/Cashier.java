package queue;
/*
 * Homework2 Queue simulation
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */

import java.text.DecimalFormat;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Cashier implements Runnable{
	
	private volatile LinkedBlockingQueue<Clients> clients = new LinkedBlockingQueue<Clients>();
	private volatile int id;
	private static int newId = 1;
	private String queue = "";
	private long runtime;
	private int totalNumberOfClients = 0;
	private double averageService = 0;
	private volatile int wait = 0;
	private double averageWaiting = 0;
	private volatile long emptyTotal = 0;
	
	public Cashier(long runtime) {
		id = newId++;
		this.runtime = runtime;
	}
	public void run() {
		try {
			while (true) {
				if (TimeUnit.SECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS) >= runtime) { // we check to see if the running time is not over
					return; // we exit the thread if the running time is over
				}
				waitForClient(); // we wait for a new client to be added to the cashier
				Thread.sleep(clients.peek().getServingTime() * 1000); // we tell the thread to "sleep" the amount of time necessary for serving the client
				deleteCurrent(); // we remove the client from the cashier
				
				
			}
		} catch (InterruptedException e) {
			System.out.println("Could not remove client from the queue!");
			
		}
	}
	public int getId() { // getter for the id of the cashier
		return this.id;
	}
	public synchronized void waitForClient() throws InterruptedException { // a method that waits until a client is send to this cashier
		while(clients.isEmpty()) {
			wait();
		}
		
		
	}
	public synchronized void addClient(Clients client) throws InterruptedException { //adds a client to the cashier
		if (this.clients.isEmpty())
			wait += client.getServingTime();
		else
			wait += client.getServingTime() + (client.getArrivalTime() - clients.peek().getArrivalTime());
		
		client.setWait(wait);
		clients.put(client);
		this.addToString(client);
		this.addToAverage(client);
		notifyAll();
	}
	public void addToAverage(Clients client) // adds the service time and the number of clients
	{
		this.totalNumberOfClients++;
		this.averageService += client.getServingTime();
	}
	public void calculateAverage(long endTime)
	{
		int x = totalNumberOfClients;
		if(totalNumberOfClients == 0)
			x = 1;
		emptyTotal = endTime - (long)averageService;
		System.out.println("Cashier nr #" + this.id + " average serving time is : " + new DecimalFormat("#.##").format(averageService / x) + " minutes; average waiting time is: " +  new DecimalFormat("#.##").format(averageWaiting / x) + " minutes, \nthe cashier was empty for: " + this.emptyTotal + " minutes" + " the total number of clients served at this cashier: " + totalNumberOfClients);
	}
	public synchronized void deleteCurrent() throws InterruptedException { // we delete the client that was served
		if (TimeUnit.SECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS) >= runtime)
			return;
		System.out.println("Time[" + Shop.getTime() + "] Deserving client#" + clients.peek().getId() + " from the cashier#" + this.id + " after waiting: " +clients.peek().getWait() + " minutes");
		averageWaiting += clients.peek().getWait();
		wait -= clients.peek().getServingTime();
		this.removeFromString(clients.poll());
		
		notifyAll();
	}
	public String getString() // return the string for the output
	{
		return this.queue;
	}
	public void removeFromString(Clients client) // update the output string
	{
		String remove = "C#" + String.valueOf(client.getId() + " ");
		queue = queue.replace(remove, "");
	}
	public void addToString(Clients client) // update the output string
	{
		queue = queue + "C#" + String.valueOf(client.getId()+ " ");
	}
	
	public synchronized int getSize() throws InterruptedException { // return the number of clients that are waiting in queue
		return clients.size();
	}
	public synchronized int getWait() throws InterruptedException {
		return this.wait;
	}

	
}
