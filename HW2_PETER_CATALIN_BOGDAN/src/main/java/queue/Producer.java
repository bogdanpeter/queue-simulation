package queue;
/*
 * Homework2 Queue simulation
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import queue.Clients;

public class Producer implements Runnable {
	private volatile List<Cashier> cashiers = new ArrayList<Cashier>();
	private int maxArrival;
	private int minArrival;
	private int maxServing;
	private int minServing;
	private long runtime;
	private int arrival = 0;
	private int max = 0;
	private long maxTime = 0;
	
	public Producer(String maxArrival, String minArrival, String maxServing, String minServing, List<Cashier> cashiers, long runtime){
		this.cashiers = cashiers;
		this.runtime = runtime;
		this.maxArrival = Integer.parseInt(maxArrival);
		this.maxServing = Integer.parseInt(maxServing);
		this.minArrival = Integer.parseInt(minArrival);
		this.minServing = Integer.parseInt(minServing);
	}	
	public void run() {
		try {
			while(true) {
				if (TimeUnit.SECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS) >= runtime) { // we check to see if the runningtime is not over
					System.out.println("Time[" + Shop.getTime() + "] Simulation done!");
					this.printAverage(); 
					return; // we stop the thread
				}
				this.arrival = getRandomArrivalTime(); // we get a random arrival time for the next client
				Thread.sleep(arrival * 1000); // we tell the thread to "sleep" until the next client is supposed to arrive
				Clients c = new Clients(getRandomServingTime(), Shop.getTime()); // we create a new client with a random serving time
				moveClientToCheckOut(c); // we add it to the appropriate cashier
				}
		
		} catch(InterruptedException e) {
			System.out.println("Error!");
		}
	}
	public synchronized void moveClientToCheckOut(Clients c)throws InterruptedException {
		Iterator<Cashier> k = cashiers.iterator();
		Cashier interm ;
		int index = 0;
		int min = cashiers.get(0).getWait();
		int maxCurrent = 0;
		while(k.hasNext())
		{
		 interm = k.next();
		 maxCurrent += interm.getSize();
		if (interm.getWait() < min) {  // we look for the cashier with the smallest waiting time
				min = interm.getWait();
				index = interm.getId()-1;	
		}
		}
		
		if (max <= maxCurrent) {
			max = maxCurrent + 1;
			maxTime = Shop.getTime();
		}
		cashiers.get(index).addClient(c);		//we add the client to the cashier and print the client in the logger
		System.out.println("Time[" + Shop.getTime() + "]" + " Client#" + c.getId() + " added to the cashier#" + (index+1) + " with a serving time of: " + c.getServingTime() + " minutes, waiting time of:" + min + " minutes \ntotal number of customers at queue: " + cashiers.get(index).getSize());
	}
	public synchronized void printAverage() { // prints the average serving and waiting time for every cashier
		Iterator<Cashier> k = cashiers.iterator();
		Cashier interm ;
		for (int i = 0; i < cashiers.size(); i++)
		{
		 interm = k.next();
		 interm.calculateAverage(Shop.getTime());		
		}
		System.out.println("Peak minute was at " + maxTime + " with a total number of customers of " + max);
	}
	public synchronized int getRandomArrivalTime() { // returns a random number between the arrival time interval
		int randomNumber = ThreadLocalRandom.current().nextInt(minArrival, maxArrival + 1);
		return randomNumber;
	}
	public synchronized int getRandomServingTime() {// returns a random number between the service time interval
		int randomNumber = ThreadLocalRandom.current().nextInt(minServing, maxServing + 1);
		return randomNumber;
	}
}

