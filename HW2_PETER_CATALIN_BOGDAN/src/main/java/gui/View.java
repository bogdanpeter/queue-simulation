package gui;
/*
 * Homework2 Queue simulation
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import queue.Cashier;


public class View extends JFrame implements Runnable
{
	private static final long serialVersionUID = 1L;
	private JTextField queueNrTf = new JTextField(20);
	private JTextField minimalArrivalTf = new JTextField(20);
	private JTextField maximualArrivalTf = new JTextField(20);
	private JTextField minimalServiceTf = new JTextField(20);
	private JTextField maximalServiceTf = new JTextField(20);
	private JTextField runtimeTf = new JTextField(20);	
	private JTextField cashierTf;
	private JButton startBtn = new JButton("Start!");
	private JButton stopBtn = new JButton("Exit!");
	private JLabel queueNrL = new JLabel("Number of queues: ");
	private JLabel minimalArrivalL = new JLabel("Minimal arrival time: ");
	private JLabel maximalArrivalL = new JLabel("Maximal arrival time: ");
	private JLabel minimalServiceL = new JLabel("Minimal service time: ");
	private JLabel maximalServiceL = new JLabel("Maximal service time: ");
	private JLabel runtimeL = new JLabel("Simulation runtime: ");
	private JLabel cashierL;
	private Font font =new Font(Font.MONOSPACED,10,20);
	private String cashier;
	private int cashiersBounds = 0;
	private JPanel panel = new JPanel();
	List<Cashier> cashiersList;
	List<JTextField> cashiers;
	// we create the interface
	public View()
	{
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setBounds(0, 0,480, 400);
		this.setTitle("Queue Simulator");
		
		panel.setLayout(null);
		panel.setBounds(0, 0, 480, 400);
		
		queueNrL.setBounds(40, 70, 290, 25);
		queueNrL.setForeground(Color.red);
		queueNrL.setFont(font);
		queueNrTf.setBounds(300,70,100, 30);
		panel.add(queueNrL);
		panel.add(queueNrTf);
		
		minimalArrivalL.setBounds(40, 110, 290, 25);
		minimalArrivalL.setForeground(Color.RED);
		minimalArrivalL.setFont(font);
		minimalArrivalTf.setBounds(300, 110, 100, 30);
		panel.add(minimalArrivalL);
		panel.add(minimalArrivalTf);
		
		maximalArrivalL.setBounds(40, 150, 290, 25);
		maximalArrivalL.setForeground(Color.RED);
		maximalArrivalL.setFont(font);
		maximualArrivalTf.setBounds(300, 150, 100, 30);
		panel.add(maximualArrivalTf);
		panel.add(maximalArrivalL);
		
		minimalServiceL.setBounds(40, 190, 290, 25);
		minimalServiceL.setForeground(Color.RED);
		minimalServiceL.setFont(font);
		minimalServiceTf.setBounds(300, 190, 100,30);
		panel.add(minimalServiceL);
		panel.add(minimalServiceTf);
		
		maximalServiceL.setBounds(40, 230, 290, 25);
		maximalServiceL.setForeground(Color.RED);
		maximalServiceL.setFont(font);
		maximalServiceTf.setBounds(300, 230, 100,30);
		panel.add(maximalServiceL);
		panel.add(maximalServiceTf);
		
		runtimeL.setBounds(40, 270, 290, 25);
		runtimeL.setForeground(Color.RED);
		runtimeL.setFont(font);
		runtimeTf.setBounds(300, 270, 100,30);
		panel.add(runtimeL);
		panel.add(runtimeTf);
		
		startBtn.setBounds(190, 310, 100, 30);
		stopBtn.setBounds(70, 310, 100, 30);
		panel.add(startBtn);
		panel.add(stopBtn);
		
		this.setVisible(true);
		this.setContentPane(panel);
	
	}
	public View(int nr, List<Cashier> cashierList)
	{
		this.cashiers = new ArrayList<JTextField>();
		this.cashiersList = cashierList;
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setLayout(null);
		this.setBounds(0, 0,1200, 800);
		this.setTitle("Queues Evolution");
		this.setLocation(0, 200);
		
		panel.setLayout(null);
		panel.setBounds(0, 0, 1200, 800);

		for (int i = 1; i <= nr; i++) {
			cashier = "Cashier #" + String.valueOf(i);
			cashierL = new JLabel(cashier);
			cashierTf = new JTextField(20);
			this.cashiers.add(cashierTf);
			cashiersBounds += 50;
			cashierTf.setBounds(150, cashiersBounds, 890, 30);
			cashierL.setBounds(50, cashiersBounds, 100, 30);
			cashierTf.setEditable(false);
			panel.add(cashierL); 
			panel.add(cashierTf);
			
		}
		SystemOutput out = new SystemOutput();
	    new Thread(out).start();
	    
		this.setVisible(true);
		this.setContentPane(panel);
	
	}

	public void run() {
		try {
			while (true) {
				Iterator<JTextField> i = cashiers.iterator();
				Iterator<Cashier> j = cashiersList.iterator();
				Cashier update = new Cashier(0);
				JTextField updateTf = new JTextField();
				while (j.hasNext())
				{
					update = j.next();
					updateTf = i.next();
					updateTf.setText(update.getString());
				}
				}
		} catch (Exception e) {
			System.out.println("Could not update clients in queue!");
			
		}
	}
	public synchronized void addClient() {
		
	}
	String 	getQueueNr()
	{
		return queueNrTf.getText();
	}
	String getMinimalArrival()
	{
		return minimalArrivalTf.getText();
	}
	String getMaximalArrival()
	{
		return maximualArrivalTf.getText();
	}
	String getMinimalService()
	{
		return minimalServiceTf.getText();
	}
	String getMaximalService()
	{
		return maximalServiceTf.getText();
	}
	String getRuntime()
	{
		return runtimeTf.getText();
	}
	void showError(String errMessage) {
		JOptionPane.showMessageDialog(this, errMessage);
		}
	void addStopListener(ActionListener stop) {
		stopBtn.addActionListener(stop);
		}
	void addStartListener(ActionListener start)
	{
		startBtn.addActionListener(start);
	}
}
class SystemOutput extends JFrame implements Runnable {
	private static final long serialVersionUID = 1L;
	private JTextArea outputTf = new JTextArea();
	JPanel panel = new JPanel();
	
	SystemOutput()
	{
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setLayout(null);
		this.setBounds(0, 0,700, 800);
		this.setTitle("Events Log");
		this.setLocation(1200, 200);
		
		panel.setLayout(new BorderLayout());
		
		JScrollPane sp = new JScrollPane(outputTf,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		panel.add(sp, BorderLayout.CENTER);

		this.setVisible(true);
		this.setContentPane(panel);
			
	}
	private void updateTextArea(final String text) {
		  SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		      outputTf.append(text);
		    }
		  });
		}
		 
		private void redirectSystemStreams()throws InterruptedException {
		  OutputStream out = new OutputStream() {
		    @Override
		    public void write(int b) throws IOException {
		      updateTextArea(String.valueOf((char) b));
		    }
		 
		    @Override
		    public void write(byte[] b, int off, int len) throws IOException {
		      updateTextArea(new String(b, off, len));
		    }
		 
		    @Override
		    public void write(byte[] b) throws IOException {
		      write(b, 0, b.length);
		    }
		  };
		 
		  System.setOut(new PrintStream(out, true));
		  System.setErr(new PrintStream(out, true));
		}
	public void run()
	{
		try {
			redirectSystemStreams();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
