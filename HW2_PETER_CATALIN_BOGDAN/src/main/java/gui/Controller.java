/*
 * Homework2 Queue simulation
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */

package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import queue.Cashier;
import queue.Producer;
import queue.Shop;

public class Controller{
	
	private View view;
	Thread t;
	
	public Controller(View view)
	{
		this.view = view;
		view.addStopListener(new StopListener());
		view.addStartListener(new StartListener());	
	}
	class StopListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
		try {
			System.exit(0);
		} catch (Exception nfex) {
			view.showError("Error!\n Could not close the Application.");		}
		}
	}
	class StartListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				int cashiersNr = Integer.parseInt(view.getQueueNr());
				long runtime = Long.parseLong(view.getRuntime()) + TimeUnit.SECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS);
				List<Cashier> cashiersList = new ArrayList<Cashier>();
				View window = new View(cashiersNr, cashiersList);
				Cashier cashier;
				for(int i = 1; i <= cashiersNr; i++)
				{
					cashier = new Cashier(runtime);
					cashiersList.add(cashier);
					new Thread(cashier).start();
				}
				Producer producer = new Producer(view.getMaximalArrival(), view.getMinimalArrival(), view.getMaximalService(), view.getMinimalService(), cashiersList, runtime);
				Shop.setTime();
				new Thread(producer).start();
				t = new Thread(window);
				t.start();
				window.setVisible(true);
			} catch (NumberFormatException nfex) {
				view.showError("Bad input! \n The input parameters should be integers.");		}
			}
	}
}
			


